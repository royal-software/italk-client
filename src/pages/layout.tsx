import { useLoaderData, Outlet } from "react-router-dom"
import { useState, createContext, useContext } from "react"
import { CiMicrophoneOn, CiHeadphones, CiSettings} from "react-icons/ci"
import Loading from "../components/utils/loading";
import Sidebar from "../components/layout/sidebar";
import { useAuth } from "../auth.provider";

const LobbyContext = createContext<{
    rooms?: any,
    setRooms?: any,
    setLoading?: any,
}>({});

export const useLobby = () => useContext(LobbyContext);

export default function Layout() {

    const { data }: any = useLoaderData()
    const { user } = useAuth()
    const [loading, setLoading] = useState<boolean>(false)
    const [rooms, setRooms] = useState<any>(data?.rooms ?? [])

    return <LobbyContext.Provider value={{ rooms, setRooms, setLoading }}>
        {loading && <Loading />}
        <div className="bg-italk-medium flex relative">
            <Sidebar />
            <div className="relative">
                <div className="fixed px-2 bottom-0 w-[240px] h-[8vh] bg-italk-dark bg-opacity-70 flex items-center space-x-3">
                    <div className="w-[35px] h-[35px] rounded-full bg-red-500 flex items-center justify-center">
                        <img src={user.avatar ?? "/icons/default-avatar.svg"} className="rounded-full" />
                    </div>
                    <div className="txt-group flex items-center justify-between space-x-3">
                        <div className="user-info">
                            <p className="text-white font-semibold text-[0.9rem] leading-4">{ user.displayName }</p>
                            <p className="text-gray-400 text-[0.8rem] leading-3">Status</p>
                        </div>
                        <div className="icons flex text-white justify-between space-x-1">
                            <CiMicrophoneOn className="text-xl"/>
                            <CiHeadphones className="text-xl"/>
                            <CiSettings className="text-xl"/>
                        </div>
                    </div>
                </div>
            </div>
            <Outlet />
        </div>
    </LobbyContext.Provider>
}