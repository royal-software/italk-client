import { useLoaderData, Outlet, useParams } from "react-router-dom"
import { useState, createContext, useContext, useEffect, useRef } from "react"
import Menubar from "../../../components/menubar/room.menubar"
import socketIOCLient from "socket.io-client";

const RoomContext = createContext<{
    room?: any,
    setRoom?: any,
    groups?: any,
    setGroups?: any,
    channels?: any,
    setChannels?: any,
}>({});

export const useRoomData = () => useContext(RoomContext);

export default function RoomLayout() {

    let { data }: any = useLoaderData()
    const { room_id } = useParams()
    let [room, setRoom] = useState<any>(data?.room)
    let [groups, setGroups] = useState<any>(data?.groups)
    let [channels, setChannels] = useState<any>(data?.channels)

    useEffect(() => {
        setRoom(data?.room)
        setGroups(data?.groups)
        setChannels(data?.channels)
    }, [room_id])

    return <RoomContext.Provider value={{ room, setRoom, groups, setGroups, channels, setChannels }}>
        <div className="flex w-full">
            <Menubar />
            <Outlet />
        </div>
    </RoomContext.Provider>
}