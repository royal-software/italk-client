import { useEffect, useRef } from "react"
import { useParams } from "react-router-dom"
import { BiPhone, BiPin, BiUserCircle } from "react-icons/bi"
import { BsFillPlusCircleFill, BsFillGiftFill, BsFillEmojiSmileFill } from "react-icons/bs"
import { MdGifBox } from "react-icons/md"
import { AiOutlineQuestionCircle } from "react-icons/ai"
import Message from "../../../components/items/message.item"
import socketIOCLient from "socket.io-client";
import { useAuth } from "../../../auth.provider"

export default function Channel() {
    const { channel_id, room_id } = useParams()
    const messageEndRef = useRef<HTMLDivElement>(null)
    const msgRef = useRef<HTMLInputElement>(null)
    const socket = useRef<any>()
    const { user } = useAuth()

    useEffect(() => {
        socket.current = socketIOCLient("http://localhost:8888", { query: { channel_id } })

        socket.current.on("new_message", (message: any) => {
            console.log(message)
        })

    }, [channel_id, room_id])

    const handleSendMsg = (event: React.SyntheticEvent<HTMLFormElement>) => {
        event.preventDefault()
        const form = event.currentTarget
        const formElements = form.elements as typeof form.elements & {
            msg: { value: string }
        }

        if (!formElements.msg.value) return
        socket.current.emit("new_message", {
            source: msgRef.current?.value,
            owner: user._id
        })
        
        form.reset()
    }

    const sendMessage = (source: any) => {
        socket.current.emit("new_message", { source, owner: user._id })
    }

    return <div className="h-full w-full bg-italk-medium">
        <div className="h-12 p-3 w-full flex justify-between items-center">
            <p className="text-gray-300">@ General</p>
            <div className="messenger_options flex text-gray-300 space-x-3">
                <BiPhone className="text-2xl" />
                <BiPin className="text-2xl" />
                <BiUserCircle className="text-2xl" />
                <input placeholder="Tìm kiếm tin nhắn" className="bg-italk-dark rounded-md h-[25px] px-2 text-gray-200 placeholder:text-gray-400" />
                <AiOutlineQuestionCircle className="text-2xl" />
            </div>
        </div>
        <div className="flex relative bottom-0 flex-col space-y-4 h-[83vh] overflow-y-auto scrollbar-thin scrollbar-thumb-italk-sdark">
            <Message data={{
                timeStamp: "25/04/2023 10:42 AM",
                user: { name: "Darkness Schoolar", id: "111" },
                content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
            }} />

            <Message data={{
                timeStamp: "25/04/2023 10:42 AM",
                user: { name: "Darkness Schoolar", id: "111" },
                content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
            }} />

            <Message data={{
                timeStamp: "25/04/2023 10:42 AM",
                user: { name: "Darkness Schoolar", id: "111" },
                content: { type: "image", source: "https://picsum.photos/720/480" }
            }} />

            <Message last={true} data={{
                timeStamp: "25/04/2023 10:42 AM",
                user: { name: "Darkness Schoolar", id: "111" },
                content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
            }} />
            <div ref={messageEndRef} className="w-full h-[50px]" />
        </div>
        <div className="form fixed bottom-0 pl-4 pb-2">
            <div className="space-x-3 px-4 bg-italk-light flex items-center justify-center rounded-2xl w-full h-[8vh]">
                <BsFillPlusCircleFill onClick={() => sendMessage("Hey hey")} className="text-gray-400 text-2xl cursor-pointer" />
                <form onSubmit={handleSendMsg}>
                    <input ref={msgRef} type="text" name="msg" placeholder="Hãy viết gì đó" className="bg-italk-light md:w-[600px] lg:w-[800px] text-gray-200 placeholder:text-gray-400 outline-none border-none" />
                </form>
                <div className="flex space-x-3">
                    <BsFillGiftFill className="text-gray-400 text-2xl" />
                    <BsFillEmojiSmileFill className="text-gray-400 text-2xl" />
                    <MdGifBox className="text-gray-400 text-2xl" />
                </div>
            </div>
        </div>
    </div>
}