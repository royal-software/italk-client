import Menubar from "../../components/menubar/lobby.menubar"
import { useEffect, useRef } from "react"
import { useLobby } from "../layout"
import { BiPhone, BiPin, BiUserCircle } from "react-icons/bi"
import { BsFillPlusCircleFill, BsFillGiftFill, BsFillEmojiSmileFill} from "react-icons/bs"
import { MdGifBox } from "react-icons/md"
import { AiOutlineQuestionCircle } from "react-icons/ai"
import Message from "../../components/items/message.item"

export default function Messenger() {

    const { user } = useLobby()
    const messageEndRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
        messageEndRef.current?.scrollIntoView()
    }, [])

    console.log(user)

    return <div className="flex justify-between w-full">
        <Menubar />
        <div className="h-full w-full bg-italk-medium">
            <div className="h-12 p-3 w-full flex justify-between items-center">
                <p className="text-gray-300">@User name</p>
                <div className="messenger_options flex text-gray-300 space-x-3">
                    <BiPhone className="text-2xl" />
                    <BiPin className="text-2xl" />
                    <BiUserCircle className="text-2xl" />
                    <input placeholder="Tìm kiếm tin nhắn" className="bg-italk-dark rounded-md h-[25px] px-2 text-gray-200 placeholder:text-gray-400" />
                    <AiOutlineQuestionCircle className="text-2xl" />
                </div>
            </div>
            <div className="flex relative bottom-0 flex-col space-y-4 h-[83vh] overflow-y-auto scrollbar-thin scrollbar-thumb-italk-sdark">
                <Message data={{
                    timeStamp: "25/04/2023 10:42 AM",
                    user: { name: "Darkness Schoolar", id: "111" },
                    content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
                }} />

                <Message data={{
                    timeStamp: "25/04/2023 10:42 AM",
                    user: { name: "Darkness Schoolar", id: "111" },
                    content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
                }} />

                <Message data={{
                    timeStamp: "25/04/2023 10:42 AM",
                    user: { name: "Darkness Schoolar", id: "111" },
                    content: { type: "image", source: "https://picsum.photos/720/480" }
                }} />

                <Message last={true} data={{
                    timeStamp: "25/04/2023 10:42 AM",
                    user: { name: "Darkness Schoolar", id: "111" },
                    content: { type: "text", source: "Easy to use, stylish placeholders stylish placeholders Just stylish placeholders Just add your desired image size (width & height) after our URL, and you'll get a random image." }
                }} />
                <div ref={messageEndRef} />
            </div>
            <div className="form fixed bottom-0 pl-4 pb-2">
                <div className="space-x-3 px-4 bg-italk-light flex items-center justify-center rounded-2xl w-full h-[8vh]">
                    <BsFillPlusCircleFill className="text-gray-400 text-2xl" />
                    <form action="">
                        <input type="text" placeholder="Hãy viết gì đó" className="bg-italk-light md:w-[600px] lg:w-[800px] text-gray-200 placeholder:text-gray-400 outline-none border-none" />
                    </form>
                    <div className="flex space-x-3">
                        <BsFillGiftFill className="text-gray-400 text-2xl" />
                        <BsFillEmojiSmileFill className="text-gray-400 text-2xl" />
                        <MdGifBox className="text-gray-400 text-2xl" />
                    </div>
                </div>
            </div>
        </div>
    </div>
}