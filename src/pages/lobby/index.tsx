import Menubar from "../../components/menubar/lobby.menubar"

export default function Lobby () {

    return <div className="flex">
        <Menubar />
        <h1>Lobby</h1>
    </div>
}