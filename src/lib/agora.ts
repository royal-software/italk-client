import {
    createClient,
    createMicrophoneAndCameraTracks,
    ClientConfig,
} from "agora-rtc-react";

export const config: ClientConfig & { appId: string, token: string } = {
    mode: "rtc", 
    codec: "vp8", 
    appId: import.meta.env.VITE_AGORA_APP_ID as string, 
    /* test token */ token: "007eJxTYDhrsWvzrHQRD5sJ+lMfLuxJzDCod+NfcpqB61xRwKEJkWcVGFINzCxSjRLNLSyNzU1SklMtDE2TDVNSU1INEi2TTE1MAtd5pTQEMjI0105kYIRCEJ+NwdDIGIgYGAA+zh4O"
};

export const useClient = createClient(config);
export const useMicrophoneAndCameraTracks = createMicrophoneAndCameraTracks();