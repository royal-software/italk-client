import axios, {AxiosInstance, AxiosResponse} from "axios"

export const MAIN_SERVER = "http://localhost:8888"
export const STORAGE_SERVER = "http://localhost:5555"

const Instance = (token?: string) => {
    const axiosInstance: AxiosInstance = axios.create({ baseURL: "http://localhost:8888" })
    // @ts-ignore
    axiosInstance.interceptors.request.use((request: AxiosRequestConfig) => {
        if (token ?? localStorage.getItem('auth')) {
            // @ts-ignore
            request.headers['authorization'] = `${localStorage.getItem('auth')}`
        }
        return request
    })
    axiosInstance.interceptors.response.use(async (response: AxiosResponse) => {
        if (response.status == 401) {
            // Khi token hết hạn hoặc ko có token mà gọi api thì lập tức logout, tự viết hàm logout thay thế cho dòng dưới
            window.location.href = "/"
        }
        return response
    }, async (error:any) => {
        if (error.response) {
            if (error.response?.status === 401) {
                // Khi token hết hạn hoặc ko có token mà gọi api thì lập tức logout, tự viết hàm logout thay thế cho dòng dưới
                window.location.href = "/"
            }
            return {
                status: error.response?.status || 400,
                data: error.response?.data
            }
        }
        return Promise.reject(error)
    })
    return axiosInstance
}

export const postSignin = (userData:any) => axios.post(MAIN_SERVER+"/oauth/signin", { ...userData })

export default Instance