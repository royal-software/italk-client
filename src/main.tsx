import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  createRoutesFromElements
} from "react-router-dom";
import './index.css'
import Instance from './lib/axios';
import AuthProvider from './auth.provider';
import Loading from './components/utils/loading';

const App = React.lazy(() => import("./App"))
const Layout = React.lazy(() => import("./pages/layout"))
const Lobby = React.lazy(() => import("./pages/lobby"))
const PrivateMessenger = React.lazy(() => import("./pages/lobby/messenger"))
const RoomLayout = React.lazy(() => import("./pages/lobby/room/layout"))
const Channel = React.lazy(() => import("./pages/lobby/room/channel"))
/**
 * loader={({ request }) =>
      fetch("https://jsonplaceholder.typicode.com/todos/1", {
        signal: request.signal,
      })
    } 
 */
const router = createBrowserRouter(createRoutesFromElements(
  <Route path="/">
    <Route index element={<App />} />
    <Route path='lobby' loader={({ request }) => {
      return Instance().get("/lobby")
    }} element={<Layout />}>
      <Route index element={<Lobby />} />
      <Route path='messenger/:uid' element={<PrivateMessenger />} />
      <Route path='room/:room_id' element={<RoomLayout />} loader={({ params }) => {
        return Instance().get("/room/get-room-info?id="+params.room_id)
    }}>
        <Route index element={<div>Room</div>} />
        <Route path=':channel_id' element={<Channel />} />
      </Route>
    </Route>

  </Route>
))

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <React.Suspense fallback={<Loading />}>
      <AuthProvider>
        <RouterProvider router={router} />
      </AuthProvider>
    </React.Suspense>
  </React.StrictMode>,
)
