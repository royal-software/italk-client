import { IRoomDetail } from '../../interfaces/room'
import { useNavigate } from 'react-router-dom'

interface IRoom extends React.HTMLAttributes<HTMLDivElement> {
    data?: IRoomDetail
}

const Room: React.FC<IRoom> = ({ data, ...rest }) => {
    const nav = useNavigate()
    return <div {...rest} onClick={() => nav(`/lobby/room/${data?._id}`)} className="cursor-pointer bg-dark-weak hover:border border-green-500 w-[46px] h-[46px] rounded-full flex items-center justify-center">
        <img src={data?.cover ?? "https://picsum.photos/46/46"} className="w-full rounded-full" alt="Avatar" />
    </div>
}

export default Room