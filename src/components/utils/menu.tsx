'use client'
import {
    ContextMenu,
    ContextMenuContent,
    ContextMenuItem,
    ContextMenuTrigger,
} from "../ui/context-menu"

import { IMenu, IMenuItem } from "../../interfaces/menu"

const Menu: React.FC<IMenu> = ({ list, children }) => {
    return <ContextMenu>
        <ContextMenuTrigger>
            {children}
        </ContextMenuTrigger>
        <ContextMenuContent className="bg-italk-sdark w-[220px] shadow-xl !border-none">

            {list.map((item, index) => <ContextMenuItem disabled={item.disabled} key={index}>
                {item.element}
            </ContextMenuItem>)}

        </ContextMenuContent>
    </ContextMenu>
}

export default Menu