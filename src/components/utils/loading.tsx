const Loading: React.FC<any> = () => {
    return <div className="fixed z-[999] top-0 left-0 w-screen h-screen bg-black bg-opacity-50 flex items-center justify-center">
        <img src="/images/loading.svg" className="w-[120px] h-[120px]" alt="loading"/>
    </div>
}

export default Loading