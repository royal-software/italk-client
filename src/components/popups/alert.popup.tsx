import { FC, useRef, useCallback } from "react"
import Overlay from "./overlay"
import { useNavigate, useParams } from "react-router-dom"
import { useRoomData } from "../../pages/lobby/room/layout"
import Instance from "../../lib/axios"

interface IAlert extends React.HTMLAttributes<HTMLDivElement> {
    setVisible: (param: any) => void
    data: {
        _id: string
        channelName: string
        room: string
        group: string
        channelType: 'text' | 'voice'
    }
}

const Alert: FC<IAlert> = ({ data, setVisible }) => {

    const { setGroups } = useRoomData()
    const nav = useNavigate()
    const { channel_id, room_id } = useParams()

    const handleDeteteChannel = async () => {
        let { data: res } = await Instance().delete("/room/delete-channel/" + data._id)
        setVisible(false)
        if (channel_id === data._id) nav('/lobby/room/'+room_id)
        setGroups(res)
    }

    return <Overlay setVisible={setVisible}>
        <div onClick={(e: any) => e.stopPropagation()} className="text-gray-200 rounded-xl w-[555px] bg-italk-light p-8">
            <p className="text-center text-xl font-semibold mb-2">Bạn muốn xóa kênh {data.channelName}?</p>
            <div className="flex justify-center items-center space-x-4">
                <button onClick={() => setVisible(false)} className="bg-italk-lighter rounded-lg px-4 py-2 font-semibold">Hủy bỏ</button>
                <button onClick={handleDeteteChannel} className="bg-red-500 rounded-lg px-4 py-2 font-semibold">Đồng ý</button>
            </div>
        </div>
    </Overlay>
}

export default Alert