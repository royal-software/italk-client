import { FC } from "react"

interface IOVerlay extends React.HTMLAttributes<HTMLDivElement> {
    setVisible: (param:any) => void
    children: React.ReactNode
}

const Overlay: FC<IOVerlay> = ({ setVisible, children }) => {
    
    return <div onClick={()=>setVisible(false)} className="fixed z-[99] w-screen h-screen bg-black bg-opacity-20 top-0 left-0 flex justify-center items-center">
        { children }
    </div>
}

export default Overlay