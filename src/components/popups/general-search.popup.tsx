import { FC, useCallback, useEffect, useRef, useState } from "react"
import Overlay from "./overlay"
import { BsReverseLayoutTextSidebarReverse } from "react-icons/bs"
import { RxSpeakerLoud } from "react-icons/rx"
import Instance from "../../lib/axios"
import { useParams } from "react-router-dom"

interface IGeneralSearch extends React.HTMLAttributes<HTMLDivElement> {
    setVisible: (param: any) => void
    header?: React.ReactNode
    body?: React.ReactNode
    footer?: React.ReactNode
}

const GeneralSearch: FC<IGeneralSearch> = ({ setVisible }) => {

    const [searchValue, setSearchValue] = useState<string>('')

    useEffect(()=> {
        handleSearch()
    }, [searchValue])

    const handleSearch = useCallback(async () => {
        console.log(`search`)
        let keyword = searchValue.substring(1)
        let prefix = searchValue[0]
        try { 
            let { data } = await Instance()
            .get(`/search`, {params: {prefix, keyword}} )
            console.log(data)
        } catch (err) {
            console.log(err)
        }
    }, [searchValue])

    return <Overlay setVisible={setVisible}>
        <div onClick={(e: any) => e.stopPropagation()} className="bg-italk-medium text-gray-200 rounded-xl w-[555px] bg-italk-light">
            <div className="popup_header bg-italk-medium rounded-xl p-4">
                <input onChange={(e:any) => setSearchValue(e.target.value)} type="text" placeholder="Tìm phòng hoặc người dùng" className="rounded-xl h-[55px] outline-none border-none bg-italk-dark w-full px-4 text-xl"/>
            </div>
            <div className="popup_body h-[220px] w-full bg-italk-medium px-4 mt-3 overflow-y-auto scrollbar-thin scrollbar-track-italk-light scrollbar-thumb-italk-dark">
                <p onClick={handleSearch} className="text-lg font-semibold text-gray-400">Các phòng của bạn</p>
                <div className="mt-3 owner_rooms text-gray-400 flex flex-col space-y-1">
                    <div className="hover:bg-italk-lighter px-3 py-2 flex w-full rounded-lg justify-between items-center">
                        <p className="text-base font-semibold">Royal software <span className="text-[0.6rem] italk-border-sm">INFO</span></p>
                        <small>300 users</small>
                    </div>
                    <div className="hover:bg-italk-lighter px-3 py-2 flex w-full rounded-lg justify-between items-center">
                        <p className="text-base font-semibold">Royal software <span className="text-[0.6rem] italk-border-sm">INFO</span></p>
                        <small>300 users</small>
                    </div>
                    <div className="hover:bg-italk-lighter px-3 py-2 flex w-full rounded-lg justify-between items-center">
                        <p className="text-base font-semibold">Royal software <span className="text-[0.6rem] italk-border-sm">INFO</span></p>
                        <small>300 users</small>
                    </div>
                    <div className="hover:bg-italk-lighter px-3 py-2 flex w-full rounded-lg justify-between items-center">
                        <p className="text-base font-semibold">Royal software <span className="text-[0.6rem] italk-border-sm">INFO</span></p>
                        <small>300 users</small>
                    </div>
                </div>
            </div>
            <div className="popup_footer bg-italk-medium rounded-b-xl px-4 py-3 ">
                <p className="text-xs font-bold text-green-500">MẸO TÌM KIẾM: 
                <span className="text-gray-400"> Bắt đầu bằng <code>@</code> với ID hoặc tên người dùng <code>#</code> với ID hoặc tên phòng</span></p>
            </div>
        </div>
    </Overlay>
}

export default GeneralSearch