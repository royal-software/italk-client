import { FC, useRef } from "react"
import Overlay from "./overlay"
import { useLobby } from "../../pages/layout"
import { BsReverseLayoutTextSidebarReverse } from "react-icons/bs"
import { RxSpeakerLoud } from "react-icons/rx"
import Instance from "../../lib/axios"
import { useParams } from "react-router-dom"
import { useRoomData } from "../../pages/lobby/room/layout"

interface ICreateGroup extends React.HTMLAttributes<HTMLDivElement> {
    setVisible: (param: any) => void
}

const CreateGroup: FC<ICreateGroup> = ({ setVisible }) => {

    const { setLoading } = useLobby()
    const { room_id } = useParams()
    const { setGroups } = useRoomData()
    const groupNameRef = useRef<HTMLInputElement>(null)

    const handleCreateGroup = async () => {
        try {
            if (!groupNameRef.current?.value) throw new Error(`err`)
            setLoading(true)
            let { data } = await Instance()
                .post(`/room/create-group`, {
                    room_id, 
                    group_name: groupNameRef.current?.value
            })

            setGroups(data)
            setVisible(false)
            setLoading(false)
        } catch (error) {
            console.log(error)
        }
    }

    return <Overlay setVisible={setVisible}>
        <div onClick={(e: any) => e.stopPropagation()} className="text-gray-200 rounded-xl w-[555px] bg-italk-light">
            <div className="popup_header p-4">
                <p className="text-xl font-semibold">Tạo nhóm mới</p>
            </div>
            <div className="popup_body p-4">
                <div className="">
                    <p className="uppercase text-xs text-gray-300 font-semibold">Channel name</p>
                    <div className="mt-2 bg-italk-dark flex space-x-2 items-center p-3 rounded-md">
                        <BsReverseLayoutTextSidebarReverse className="text-lg text-white" />
                        <input ref={groupNameRef} type="text" className="outline-none border-none placeholder:text-gray-400 w-full bg-italk-dark pl-1" placeholder="new-channel" />
                    </div>
                </div>
            </div>
            <div className="popup_footer p-6 rounded-b-xl bg-italk-medium flex justify-end items-center space-x-6">
                <button onClick={() => setVisible(false)} className="text-white font-semibold">Cancel</button>
                <button onClick={handleCreateGroup} className="text-white font-semibold px-3 py-2 rounded-md bg-button-success">Create Group</button>
            </div>
        </div>
    </Overlay>
}

export default CreateGroup