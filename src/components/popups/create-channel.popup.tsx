import { FC, useRef } from "react"
import Overlay from "./overlay"
import { BsReverseLayoutTextSidebarReverse } from "react-icons/bs"
import { RxSpeakerLoud } from "react-icons/rx"
import Instance from "../../lib/axios"
import { useParams } from "react-router-dom"

interface ICreateChannel extends React.HTMLAttributes<HTMLDivElement> {
    group: any
    setGroups: (param: any) => void
    setVisible: (param: any) => void
}

const CreateChannel: FC<ICreateChannel> = ({ setGroups, setVisible, group }) => {

    const { room_id } = useParams()
    const channelNameRef = useRef<HTMLInputElement>(null)

    const handleCreateChannel = async () => {
        try {
            let { data } = await Instance()
                .post(`/room/create-channel`, {
                    groupId: group._id, 
                    roomId: room_id, 
                    channelName: channelNameRef.current?.value, 
                    channelType: 'text'
            })
            setVisible(false)
            setGroups(data)
        } catch (error) {
            console.log(error)
        }
    }

    return <Overlay setVisible={setVisible}>
        <div onClick={(e: any) => e.stopPropagation()} className="text-gray-200 rounded-xl w-[555px] bg-italk-light">
            <div className="popup_header p-4">
                <p className="text-xl font-semibold">Tạo kênh mới</p>
                <small>Trong {group.groupName as string}</small>
            </div>
            <div className="popup_body p-4">
                <div className="mt-2 channel-type flex flex-col space-y-2">
                    <p className="uppercase text-xs text-gray-300 font-semibold">Channel type</p>
                    <div className="flex items-center space-x-4 bg-italk-lighter py-2 px-3">
                        <BsReverseLayoutTextSidebarReverse className="text-2xl text-gray-400" />
                        <div className="txt-group">
                            <p>Text</p>
                            <small className="text-gray-400">Gửi tin nhắn, hình ảnh, GIFs, video, biểu cảm...</small>
                        </div>
                    </div>
                    <div className="flex items-center space-x-4 bg-italk-medium py-2 px-3">
                        <RxSpeakerLoud className="text-2xl text-gray-400" />
                        <div className="txt-group">
                            <p>Voice</p>
                            <small className="text-gray-400">Kết nối và tán ngẫu với mọi người...</small>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                    <p className="uppercase text-xs text-gray-300 font-semibold">Channel name</p>
                    <div className="mt-2 bg-italk-dark flex space-x-2 items-center p-3 rounded-md">
                        <BsReverseLayoutTextSidebarReverse className="text-lg text-white" />
                        <input ref={channelNameRef} type="text" className="outline-none border-none placeholder:text-gray-400 w-full bg-italk-dark pl-1" placeholder="new-channel" />
                    </div>
                </div>
            </div>
            <div className="popup_footer p-6 rounded-b-xl bg-italk-medium flex justify-end items-center space-x-6">
                <button onClick={()=>setVisible(false)} className="text-white font-semibold">Cancel</button>
                <button onClick={handleCreateChannel} className="text-white font-semibold px-3 py-2 rounded-md bg-button-success">Create Channel</button>
            </div>
        </div>
    </Overlay>
}

export default CreateChannel