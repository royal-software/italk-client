import { FC, useState } from "react"
import Friend from "../items/friend.item"
import GeneralSearch from "../popups/general-search.popup"

interface IMenubar extends React.HTMLAttributes<HTMLDivElement> { }

const Menubar: FC<IMenubar> = ({ ...rest }) => {

    const [visibleSearch, setVisibleSearch] = useState<boolean>(false)

    return <div className="min-w-[240px] bg-italk-dark">
        { visibleSearch && <GeneralSearch setVisible={setVisibleSearch}/> }
        <div className="p-3 flex items-center h-12">
            <button onClick={()=>setVisibleSearch(true)} className="w-full bg-italk-medium p-1 text-gray-300 text-[0.9rem]">Tìm kiếm phòng hoặc bạn bè</button>
        </div>
        <div className="max-h-[84vh] overflow-auto scrollbar-none p-3 flex flex-col space-y-2">
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
            <Friend />
        </div>

    </div>
}

export default Menubar