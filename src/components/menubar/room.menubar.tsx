import { FC, useState } from "react"
import { MdOutlinePublic } from "react-icons/md"
import { IMenuItem } from "../../interfaces/menu"
import { IChannel } from "../../interfaces/channel"
import { TiStarburst, TiPlus } from "react-icons/ti"
import Channel from "../items/channel.item"
import CreateChannel from "../popups/create-channel.popup"
import CreateGroup from "../popups/create-group.popup"
import { useRoomData } from "../../pages/lobby/room/layout"
import Alert from "../popups/alert.popup"

interface IMenubar extends React.HTMLAttributes<HTMLDivElement> { }

interface IMenubarChannel extends React.HTMLAttributes<HTMLDivElement> {
    channel: IChannel
}

const MenubarChannel: FC<IMenubarChannel> = ({ channel }) => {
    const [visibleConfirm, setVisibleConfirm] = useState<boolean>(false)

    return <div>
        {visibleConfirm && <Alert setVisible={setVisibleConfirm} data={channel} />}
        <Channel visibleConfirm={visibleConfirm} setVisibleConfirm={setVisibleConfirm} data={channel} />
    </div>
}

const Menubar: FC<IMenubar> = ({ }) => {

    const { room, groups, setGroups } = useRoomData()
    const [visibleChannelCreator, setVisibleChannelCreator] = useState<boolean>(false)
    const [visibleGroupCreator, setVisibleGroupCreator] = useState<boolean>(false)
    const [currentGroup, setCurrentGroup] = useState<any>()

    const handleOpenChannelCreator = (item: any) => {
        setCurrentGroup(item)
        setVisibleChannelCreator(true)
    }

    return <div className="min-w-[240px] bg-italk-dark">
        {visibleChannelCreator && <CreateChannel setGroups={setGroups} setVisible={setVisibleChannelCreator} group={currentGroup as string} />}
        {visibleGroupCreator && <CreateGroup setVisible={setVisibleGroupCreator} />}
        <div className="px-4 py-2 h-36" style={{ backgroundImage: `url(/images/bg-sm.jpg)` }}>
            <div className="flex items-center text-white space-x-1">
                <TiStarburst className="text-xl text-green-500" />
                <span className="text-lg font-semibold">{room.roomName}</span>
            </div>
            <div className="rounded-xl bg-italk-dark flex items-center justify-center space-x-1 p-1 w-16">
                <MdOutlinePublic className="text-sm text-gray-300" />
                <p className="text-xs text-gray-300">Public</p>
            </div>
        </div>
        <div className="max-h-[84vh] overflow-auto scrollbar-none p-3 flex flex-col space-y-2">

            {groups?.length > 0 && groups?.map((item: any, index: number) => {
                return <div key={index} >
                    <div className="my-1 flex justify-between items-center text-gray-300">
                        <span className="text-xs font-semibold uppercase">{item.groupName}</span>
                        <TiPlus className="cursor-pointer" onClick={() => handleOpenChannelCreator(item)} />
                    </div>
                    {item.channels.map((channel: IChannel) => {
                        return <MenubarChannel key={channel._id} channel={channel} />
                    })}
                </div>
            })}

            <div onClick={() => setVisibleGroupCreator(true)} className="cursor-pointer flex space-x-1 items-center text-gray-400">
                <TiPlus /><span className="text-xs font-semibold uppercase">Tạo group</span>
            </div>
        </div>

    </div>
}


export default Menubar