import React, { useState, useRef } from "react"
import Menu from "../utils/menu"
import { useNavigate } from "react-router-dom"
import { IMenuItem } from "../../interfaces/menu"
import { AiOutlineCompass, AiOutlineUserAdd, AiOutlineSetting, AiOutlinePlus, AiFillSetting } from "react-icons/ai"
import { RxCopy, RxExit } from "react-icons/rx"
import Room from "../utils/room"

import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "../ui/select"
import { useLobby } from "../../pages/layout"
import Instance from "../../lib/axios"
import { useAuth } from "../../auth.provider"

interface ISidebar extends React.HTMLAttributes<HTMLDivElement> { }

const Sidebar: React.FC<ISidebar> = ({ ...props }) => {

    const nav = useNavigate()
    const { user } = useAuth()
    console.log(user)
    const { rooms, setRooms, setLoading } = useLobby()
    const [showModal, setShowModal] = useState<boolean>(false)
    const roomNameRef = useRef<HTMLInputElement>(null)

    let userList: Array<IMenuItem> = [
        {
            element: <div className="flex items-center space-x-2 text-white font-semibold">
                <AiOutlineUserAdd className="text-lg" />
                <p>Invite</p>
            </div>
        },
        {
            element: <div className="flex items-center space-x-2 text-white font-semibold">
                <AiOutlineSetting className="text-lg" />
                <p>Setting Room Profile</p>
            </div>
        },
        {
            element: <div className="flex items-center space-x-2 text-white font-semibold">
                <RxCopy className="text-lg" />
                <p>Copy Room ID</p>
            </div>
        },
        {
            element: <div className="flex items-center space-x-2 text-red-400 font-semibold">
                <RxExit className="text-lg" />
                <p>Leave</p>
            </div>
        },
    ]

    const handleCreateRoom = async () => {
        try {
            if (!roomNameRef.current?.value) throw new Error("Room name is invalid")
            setLoading(true)
            let { data } = await Instance().post("/room/create-room", { 
                status: "public", 
                roomName: roomNameRef.current?.value
            })
            console.log(data)
            setShowModal(false)
            setRooms([...rooms, data])
            setLoading(false)
        } catch (err) {
            console.log(err)
        } finally {
            console.log(rooms)
        }
    }

    return <div {...props} className="min-h-screen space-y-[18px] py-4 w-[70px] bg-italk-sdark flex flex-col items-center">

        {showModal && <div className="fixed z-[999] top-0 left-0 w-screen h-screen bg-black bg-opacity-30 flex justify-center items-center">
            <div className="w-[30%] h-[500px] bg-white rounded-lg p-6 flex flex-col">
                <h1 className="text-center text-xl font-bold mb-2">Tạo phòng chat</h1>
                <p className="text-center text-sm">Phòng của bạn là nơi bạn và bạn bè được kết nối, hãy bắt đầu cuộc trò chuyện nào</p>

                <div className="mt-4 list flex-col space-y-4">
                    <div className="cursor-pointer bg-gray-300 flex space-x-4 items-center rounded-lg border-[0.3px] border-solid border-gray-400 w-full px-4 py-2">
                        <img src={"/icons/public.svg"} width={50} height={50} alt="public" />
                        <div>
                            <p className="font-semibold text-gray-600">Tạo phòng công khai</p>
                            <small>Tất cả mọi người có thể vào phòng, phòng của bạn sẽ hiển thị ở Community</small>
                        </div>
                    </div>
                    <div className="cursor-pointer flex space-x-4 items-center rounded-lg border-[0.3px] border-solid border-gray-400 w-full px-4 py-2">
                        <img src={"/icons/private.svg"} width={50} height={50} alt="private" />
                        <div>
                            <p className="font-semibold text-gray-600">Tạo phòng riêng tư</p>
                            <small>Chỉ bạn và những người được bạn mời mới có thể vào phòng</small>
                        </div>
                    </div>
                    <input ref={roomNameRef} type="text" placeholder="Tên phòng" className="rounded-lg h-[44px] w-full outline-none border-[0.3px] border-solid border-gray-400 px-4" />
                    <Select>
                        <SelectTrigger className="rounded-lg h-[44px] border-[0.3px] border-solid border-gray-400 w-full">
                            <SelectValue placeholder="Loại hình" />
                        </SelectTrigger>
                        <SelectContent>
                            <SelectItem value="Gamming">Gamming</SelectItem>
                            <SelectItem value="Club">Club</SelectItem>
                            <SelectItem value="sci">Khoa học</SelectItem>
                            <SelectItem value="other">Khác</SelectItem>
                        </SelectContent>
                    </Select>
                    <div className="flex justify-end space-x-2">
                        <button onClick={() => setShowModal(false)} className="rounded-lg border-[0.3px] border-solid border-gray-400 text-gray-500 px-4 py-2">Hủy</button>
                        <button onClick={handleCreateRoom} className="rounded-lg bg-gray-500 text-white px-4 py-2">Tạo phòng</button>
                    </div>
                </div>
            </div>
        </div>}

        {rooms?.length > 0 && rooms?.map((item: any, index: number) => <Menu key={index} list={[{
            element: <div className="flex items-center space-x-2 text-red-400 font-semibold">
                <p className="text-lg font-semibold">{item?.roomName}</p>
            </div>, disabled: true
        },...userList]}>
            <Room data={item}/>
        </Menu>)}


        <Menu
            list={[
                { element: <p className="text-white">+ Join a server</p> },
                { element: <p onClick={() => setShowModal(true)} className="cursor-pointer text-white">+ Create a server</p> }]}>
            <div className="bg-italk-dark hover:bg-dark-basic w-[48px] h-[48px] rounded-full flex items-center justify-center">
                <AiOutlinePlus className="text-2xl text-green-500" />
            </div>
        </Menu>
        <div onClick={() => nav("/lobby/discovery")} className="cursor-pointer bg-italk-dark hover:bg-dark-basic w-[48px] h-[48px] rounded-full flex items-center justify-center">
            <AiOutlineCompass className="text-2xl text-green-500" />
        </div>
    </div>
}

export default Sidebar