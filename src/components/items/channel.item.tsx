import { FC, useState } from "react"
import { useNavigate } from "react-router-dom"
import Menu from "../utils/menu"
import { BsReverseLayoutTextSidebarReverse } from "react-icons/bs"
import { RxSpeakerLoud }  from "react-icons/rx"
import { IMenuItem } from "../../interfaces/menu"

interface IChannel extends React.HTMLAttributes<HTMLDivElement> {
    visibleConfirm: any
    setVisibleConfirm: (param: any) => void
    data: {
        _id: string
        channelName: string
        channelType: 'text' | 'voice'
    }
}

const Channel:FC<IChannel> = ({ visibleConfirm, setVisibleConfirm, data, ...rest }) => {
    const nav = useNavigate()

    const handleClick = () => {
        console.log(`click to open delete popup, after: `, visibleConfirm)
        setVisibleConfirm(true)
    }

    const menu: Array<IMenuItem> = [
        { element: <p className="text-white">Setting</p> },
        { element: <p className="text-white">Mute Channel</p> },
        { element: <p className="text-white">Copy Link</p> },
        { element: <p onClick={handleClick} className="text-red-400">Delete Channel</p> },
    ]

    return <Menu list={menu}>
        <div onClick={()=>nav(data._id)} className="text-gray-300 hover:bg-italk-light rounded-lg px-2 py-1 cursor-pointer flex space-x-3 items-center">
            { data.channelType==="text"? <BsReverseLayoutTextSidebarReverse />: <RxSpeakerLoud />}
            <p className="text-gray-300">{ data.channelName }</p>
        </div>
    </Menu>
}

export default Channel