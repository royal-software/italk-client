import { IMenuItem } from "../../interfaces/menu"
import Menu from "../utils/menu"

type ContentType = "video" | "image" | "text"

interface IMessage extends React.HTMLAttributes<HTMLDivElement> {
    last?: boolean,
    data: {
        timeStamp: any,
        user: { id: string, name:string, avatar?: string },
        content: { type: ContentType, source: string }
    }
}

const Message: React.FC<IMessage> = ({ data, last }) => {

    const messageMenuList:Array<IMenuItem> = [
        { element: <p className="text-white">Cảm xúc</p> },
        { element: <p className="text-white">Chỉnh sửa</p> },
        { element: <p className="text-white">Trả lời</p> },
        { element: <p className="text-red-500">Xóa</p> }
    ]

    const renderContent = {
        "video": <video src={data.content.source}></video>,
        "image": <img className="rounded-xl max-w-[700px] max-h-[400px]" src={data.content.source}></img>,
        "text": <p className="max-w-[65vw] text-white text-[0.9rem]">{data.content.source}</p>
    }

    return <Menu list={messageMenuList}>
        <div style={{position:last?'sticky':'static'}} className="flex px-4 py-2 hover:bg-italk-dark hover:bg-opacity-50 space-x-4">
            <img src={data.user.avatar ?? "https://picsum.photos/40/40"} className="rounded-full w-[40px] h-[40px]" alt="" />
            <div className="content">
                <div className="flex space-x-2 items-center">
                    <p className="text-base text-white font-semibold">{data.user.name}</p>
                    <small className="text-gray-400">{data.timeStamp}</small>
                </div>
                { renderContent[data.content.type] }
            </div>
        </div>
    </Menu>
}

export default Message