import { FC } from "react"
import { useNavigate } from "react-router-dom"
import Menu from "../utils/menu"
import { IMenuItem } from "../../interfaces/menu"

interface IFriend extends React.HTMLAttributes<HTMLDivElement> {
    data?: any
}

const Friend:FC<IFriend> = ({ data, ...rest }) => {

    const nav = useNavigate()

    const friendMenu:Array<IMenuItem> = [
        { element: <p className="text-white">Profile</p> },
        { element: <p className="text-white">Call</p> },
        { element: <p className="text-white">Unfirend</p> },
        { element: <p className="text-white">Block</p> },
    ]

    return <Menu list={friendMenu}>
        <div onClick={()=>nav("/lobby/messenger/123123")} className="hover:bg-italk-light rounded-lg px-2 py-1 cursor-pointer flex space-x-3 items-center">
            <img src="https://picsum.photos/35/35" className="w-[35px] h-[35px] rounded-full border border-green-500" alt="" />
            <p className="text-gray-300">User name</p>
        </div>
    </Menu>
}

export default Friend