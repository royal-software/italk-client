
import Firebase from './lib/firebase'
import { FcGoogle } from "react-icons/fc"
import { FaUserCircle } from "react-icons/fa"
import { useNavigate } from 'react-router-dom'
import { useRef, useState } from "react"
import { getAuth, signInWithPopup, sendEmailVerification, GoogleAuthProvider, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth"
import { postSignin } from './lib/axios'
import Loading from './components/utils/loading'
import { useAuth } from './auth.provider'

export default function Home() {

  Firebase()

  const [loading, setLoading] = useState(false)
  const { setUser } = useAuth() // su dung
  const nav = useNavigate()

  const emailRef = useRef<HTMLInputElement>(null)
  const passwordRef = useRef<HTMLInputElement>(null)
  const provider = new GoogleAuthProvider()
  const auth = getAuth()

  const signIn = async () => {
    try {
      setLoading(true)
      let result: any = await signInWithPopup(auth, provider)
      // @ts-ignore
      localStorage.setItem('auth', result.user.accessToken)
      let { data } = await postSignin({
        email: result.user.email as string,
        displayName: result.user.displayName as string,
        avatar: result.user.photoURL as string,
        uid: result.user.uid
      })
      setUser(data)
      nav("/lobby")
    } catch (err) {
      setLoading(false)
    }
  }

  // const signIn2 = async () => {
  //   let result = await signInWithEmailAndPassword(auth, email, password)
  //   console.log(result)
  // }

  const signUp = async () => {
    try {
      let result = await createUserWithEmailAndPassword(auth,
        emailRef.current?.value as string,
        passwordRef.current?.value as string
      ); let sender = await sendEmailVerification(result.user)
      console.log(result)
      console.log(60, sender)
    } catch {}
  }

  return (
    <main className="flex min-h-screen flex-col space-y-4 items-center justify-center p-24 bg-black bg-opacity-70">
      {loading && <Loading />}
      <form className="flex flex-col space-y-2">
        <div className="flex items-center justify-center">
          <h1 className="text-3xl my-2 font-bold text-white">Đăng Nhập</h1>
        </div>
        <input disabled ref={emailRef} placeholder="✉️ Email" className="w-[375px] h-[40px] shadow-2xl bg-black bg-opacity-50 border-t-[0.3px] border-solid border-gray-500 px-3 text-gray-300 placeholder:text-gray-400 rounded-lg" />
        <input disabled ref={passwordRef} placeholder="🔑 Mật khẩu" className="w-[375px] h-[40px] shadow-2xl bg-black bg-opacity-50 border-t-[0.3px] border-solid border-gray-500 px-3 text-gray-300 placeholder:text-gray-400 rounded-lg" />
      </form>

      <button onClick={signUp} className='w-[375px] flex space-x-3 items-center px-4 py-2 rounded-xl bg-white'>
        <FaUserCircle className='text-2xl' />
        <p className='text-base text-dark-weak font-semibold'>Đăng ký hoặc đăng nhập bằng tài khoản</p>
      </button>
      <button onClick={signIn} className='w-[375px] flex space-x-3 items-center px-4 py-2 rounded-xl bg-white'>
        <FcGoogle className='text-2xl' />
        <p className='text-base text-dark-weak font-semibold'>Đăng nhập với tài khoản Google</p>
      </button>
      {/* <button onClick={auth.signOut}>SIGN OUT</button> */}
    </main>
  )
}
