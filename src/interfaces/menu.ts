export interface IMenuItem {
    disabled?: boolean
    element: React.ReactNode
}

export interface IMenu extends React.HTMLAttributes<HTMLDivElement> {
    children: React.ReactNode,
    list: Array<IMenuItem>
}
