export interface IChannel {
    _id: string
    channelName: string
    room: string
    group: string
    channelType: 'text' | 'voice'
}