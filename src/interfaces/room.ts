export interface IRoomDetail {
    cover: string
    createdAt: Date
    host: string
    managers: []
    roomName: string
    status: string
    updatedAt: string
    users: Array<string|any>
    __v: number|any
    _id: string
}