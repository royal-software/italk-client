import { useEffect, useState, createContext, useContext } from "react"
import axios from "axios";
import Instance from "./lib/axios";
const AuthContext = createContext<{ user?: any, setUser?: any }>({});
// trong JS ko can phai khia bao type, goi nhu sau: AuthContext = createContext({});

export default function AuthProvider({ children }:any) {

    const [user, setUser] = useState<any>(null)
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        Instance().get("/profile").then(res => {
            setUser(res.data)
        })
    }, [])

    return <AuthContext.Provider value={{ user, setUser }}>
        {/* AuthContext.Provider có nhiệm vụ phân phát các state tới toàn bộ component thấp hơn nó, ở đây bao gồm user và setUser */}
        { children }
    </AuthContext.Provider>
}

export const useAuth = () => useContext(AuthContext);
// dùng cái này ở bất cứ đâu để gọi và set state